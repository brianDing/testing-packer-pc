variable "db_endpoint"{
    default = ""
}

locals { 
    packer_start_time = formatdate("YYYY-MM-DD-hhmm", timestamp())
}

source "amazon-ebs" "petclinic"{
    ami_name =  "petclinic-AMI-${local.packer_start_time}"
    instance_type = "t2.micro"
    region = "eu-west-1"
    source_ami = "ami-063d4ab14480ac177"
    ssh_username = "ec2-user"
    tags= {
        Name = "petclinic-ami"
    }
}

build {
    sources = ["source.amazon-ebs.petclinic"]

    provisioner "file"{
        source = "./petclinic_ami/provision_install.sh"
        destination = "/home/ec2-user/"
    }

    provisioner "file"{
        source = "./petclinic_ami/create_user.sql"
        destination = "/home/ec2-user/"
    }

    provisioner "file"{
        source = "./petclinic_ami/petclinic.init"
        destination = "/home/ec2-user/"
    }

    provisioner "shell" {
        inline = [
            "echo 'export db_host=${var.db_endpoint}' >> ~/.bash_profile",
            "echo 'export db_user=admin' >> ~/.bash_profile",
            "echo 'export db_pw=adminpassword' >> ~/.bash_profile",
            "source ~/.bash_profile",
            "cd /home/ec2-user",
            "chmod +x provision_install.sh provision_db.sh",
            "./provision_install.sh",
        ]
    }
}
