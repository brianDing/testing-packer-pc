resource "aws_security_group" "sg-petclinic-webserver_A" {
    name = "sg_${var.environment_tag}_petclinic_A"
    vpc_id = var.vpc_id
    description = "Allow 80"
    ingress {
        from_port   = 8080
        to_port     = 8080
        protocol    = "tcp"
        security_groups = [var.sg_lb_A_id]
    }
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        security_groups = [var.sg_jenkins_master_id, var.sg_jenkins_worker_id]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        "Environment" = var.environment_tag
        "Name"        = "sg-${var.environment_tag}-petclinic-A"
    }
}


# resource "aws_security_group" "sg-petclinic-webserver_B" {
#     name = "sg_${var.environment_tag}_petclinic_B"
#     vpc_id = var.vpc_id
#     description = "Allow 80"
#     ingress {
#         from_port   = 80
#         to_port     = 80
#         protocol    = "tcp"
#         security_groups = [var.sg_lb_B.id]
#     }
#     ingress {
#         from_port   = 22
#         to_port     = 22
#         protocol    = "tcp"
#         security_groups = [var.sg_jenkins_master_id, var.sg_jenkins_worker_id]
#     }
#     egress {
#         from_port   = 0
#         to_port     = 0
#         protocol    = "-1"
#         cidr_blocks = ["0.0.0.0/0"]
#     }
#     tags = {
#         "Environment" = var.environment_tag
#         "Name"        = "sg-${var.environment_tag}-petclinic_B"
#     }
# }

resource "aws_instance" "petA_server_1" {
  ami = var.base_ami
  instance_type = var.instance_type
  availability_zone = var.zone_A
  associate_public_ip_address = true
  subnet_id = var.subnet_id
  security_groups = [aws_security_group.sg-petclinic-webserver_A.id]
  key_name = "BTKEY"
  tags = {
      "Name" = "bt-petclinic-A1"
  }
}

# resource "aws_instance" "petA_server_2" {
#   ami = var.base_ami
#   instance_type = var.instance_type
#   availability_zone = var.zone_A
#   associate_public_ip_address = true
#   subnet_id = aws_subnet.subnet_public.id
#   security_groups = [aws_security_group.sg-petclinic-webserver_A.id]
#   key_name = "BTKEY"
#   tags = {
#       "Name" = "bt-petclinic-A2"
#   }
# }


# resource "aws_instance" "petB_server_1" {
#   ami = var.base_ami
#   instance_type = var.instance_type
#   availability_zone = var.zone_B
#   associate_public_ip_address = true
#   subnet_id = var.subnet_id
#   security_groups = [aws_security_group.sg-petclinic-webserver_B.id]
#   key_name = "BTKEY"
#   tags = {
#       "Name" = "bt-petclinic-B1"
#   }
# }

# resource "aws_instance" "petB_server_2" {
#   ami = var.base_ami
#   instance_type = var.instance_type
#   availability_zone = var.zone_B
#   associate_public_ip_address = true
#   subnet_id = aws_subnet.subnet_public.id
#   security_groups = [aws_security_group.sg-petclinic-webserver_B.id]
#   key_name = "BTKEY"
#   tags = {
#       "Name" = "bt-petclinic-B2"
#   }
# }
