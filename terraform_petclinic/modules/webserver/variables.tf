# Defining variables
variable "environment_tag" {
    description = "group name tag"
    default = "bt"
}


variable "vpc_id" {
    
}

variable "subnet_id"{
    
}


variable "sg_lb_A_id"{

}


variable "sg_jenkins_master_id" {

}

variable "sg_jenkins_worker_id" {

}

variable "base_ami" {
    description = "Amazon Linux AMI"
    default = "ami-063d4ab14480ac177"
}

variable "instance_type" {
    description = "Default instance type"
    default = "t2.micro"
}

variable "zone_A"{
    description = "Availablility zone A"
    default = "eu-west-1a"
}

variable "zone_B"{
    description = "Availablility zone b"
    default = "eu-west-1b"
}
