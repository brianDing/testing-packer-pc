## Loadbalancer SG
resource "aws_security_group" "sg_lb_A" {
    name = "sg_${var.environment_tag}_loadbalancer"
    vpc_id = var.vpc_id
    description = "Allow all access and SSH from Jenkins"
    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        security_groups = [var.sg_jenkins_master_id, var.sg_jenkins_worker_id]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        "Environment" = var.environment_tag
        "Name"        = "sg-${var.environment_tag}-loadbalancer-A"
    }
}


resource "aws_security_group" "sg_lb_B" {
    name = "sg_${var.environment_tag}_loadbalancer"
    vpc_id = var.vpc_id
    description = "Allow all access and SSH from Jenkins"
    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        security_groups = [var.sg_jenkins_master_id, var.sg_jenkins_worker_id]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        "Environment" = var.environment_tag
        "Name"        = "sg-${var.environment_tag}-loadbalancer-B"
    }
}


resource "aws_instance" "petA_load_balancer" {
  ami = var.base_ami
  instance_type = var.instance_type
  availability_zone = var.zone_A
  associate_public_ip_address = true
  subnet_id = var.subnet_id_A
  security_groups = [aws_security_group.sg_lb_A.id]
  key_name = "BTKEY"
  tags = {
      "Name" = "bt-petclinic-lb-1"
  }
}

resource "aws_instance" "petB_load_balancer" {
  ami = var.base_ami
  instance_type = var.instance_type
  availability_zone = var.zone_B
  associate_public_ip_address = true
  subnet_id = var.subnet_id_B
  security_groups = [aws_security_group.sg_lb_B.id]
  key_name = "BTKEY"
  tags = {
      "Name" = "bt-petclinic-lb-2"
  }
}
