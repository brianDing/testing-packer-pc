# Assumption that after creating infrastructure
# A tfvars file created by using terraform output is scp into the jenkins worker node

cd ~/assessment-3/terraform_db
terraform apply -var-file="infrastructure_info.tfvars"

# inside infrastructure_info.tfvars
    # vpc_id
    # sg_web_id
    # sg_jenkins_master_id
    # sg_jenkins_worker_id
    # private_subnet_id_1a
    # private_subnet_id_1b
    # private_subnet_id_1c


packer build -var db_endpoint=$(terraform output -raw db_endpoint) ../petclinic_ami/packer_create_AMI.json


cd ~/assessment-3/terraform_petclinic
terraform apply -var-file="infrastructure_info.tfvars"

packer build -var 'db_endpoint=brian-test-database.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com' ./petclinic_ami/packer_create_AMI.json

packer build -var 'db_endpoint=brian-test-database.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com' ./petclinic_ami/packer_create_AMI.pkr.hcl
